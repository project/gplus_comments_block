<?php

/**
 * @file
 * Template for count of Google+ Comments.
 *
 * Variable available:
 * - $url.
 */
?>
<div class="g-commentcount" data-href="<?php print $url; ?>"></div>
